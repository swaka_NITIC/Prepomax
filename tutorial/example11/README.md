# Heat transfer in insulated pipeline
パイプ外側に断熱材を張った２重パイプ（外径57mm, 内径50mm、断熱材厚さ28㎜、長さ200mm）の熱伝導解析を行います。

# 形状モデルの読み込みと解析実行手順
1. PrePoMaxを起動し、[mm,  ton,  s, °C]単位のプロジェクトファイルを新規作成します（File > New）。  

2. ダウンロードしたGeometryファイルの中から"Example 11 - pipeline.step"をimportします（File > import）。
　次に、Meshing Parametersの設定を変更して、最大要素サイズ(Max element size)を変更（パイプ部3mm、断熱材部4mm）し、それぞれメッシュ生成を行ってください（テキストは断熱材部分のメッシュサイズ2mmとあるが、図から4㎜と予想されます）。

    <img src="image/example11_01.png" width=30%>  

3. 材料を2種類指定します。Materialsを右クリック>Create>Themal conductivityで熱伝導率を指定します（名前Pipe_material, 45mW/(mm℃)),(名前Insulation＿material, 0.05mW/(mm℃))。PipeとInsulation部分にSectionを設定し、対応した材料を指定してください。　

    <img src="image/example11_02.png" width=30%>  
    <img src="image/example11_03.png" width=30%>  

4. InsulaionとPipe間の面に対し、”tie constraint”を設定します。Insulation_pipeの外側surfaceをTie1, Pipeの内側surafaceをTie2として、それぞれsurfaceを作成し、これらをmaster/slaveとして、Constraintsを右クリック＞Createで、”tie constraint”を作成します。

    <img src="image/example11_04.png" width=30%>  

5. Stepsで”heat transfer step"を定義します。(Stepsを右クリック>Create>type "heat transfer analysis"を選択)します。その他の設定はdefaultのままです。  

6. 熱的な荷重条件（loads）を設定します。まず断熱材外側面にsink温度20℃、フィルム係数（単位面積当たりの熱伝達係数）0.015 mW/(mm2·°C)を指定します。パイプ内側面にsink温度-30℃、フィルム係数（単位面積当たりの熱伝達係数）0.8 mW/(mm2·°C)を指定します。

    <img src="image/example11_05.png" width=30%>
    <img src="image/example11_06.png" width=30%>

6. 解析を実行します（Analysis-1を右クリック＞Runを選択）。 解析的には、内側表面温度-29.83℃、外側表面温度16.05℃となります。解析的にどうかQueryを使って調べましょう。 

    <img src="image/example11_07.png" width=30%>  


