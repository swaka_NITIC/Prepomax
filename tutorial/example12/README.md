# Termomechanical analysis of a bimetallic strip
異なる熱膨張係数を持った異種金属板材（120x20x4mm）を張り合わせたバイメタルの変形解析を行います。

# 形状モデルの読み込みと解析実行手順
1. PrePoMaxを起動し、[mm,  ton,  s, °C]単位のプロジェクトファイルを新規作成します（File > New）。  

2. ダウンロードしたGeometryファイルの中から"Example 12 - bimetallic strip.step"をimportします（File > import）。
　次に、Meshing Parametersの設定を変更して、最大要素サイズ(Max element size)を2㎜に変更し、それぞれメッシュ生成を行ってください(マニュアルは1mm指定ですがですが...細かすぎると解析できないようです)。

    <img src="image/example12_01.png" width=30%>  

3. 材料を2種類指定します。
- Materialsを右クリック>Create>Elasticiy で名前Copper, ヤング率130000 MPa, ポアソン比0.34, 熱伝導率 385 mW/(mm℃), Thermal expansion 17e-6 (1/℃), zero temperature 20℃  
-  同様に名前Steel, ヤング率210000 MPa, ポアソン比0.3, 熱伝導率 45 mW/(mm℃), Thermal expansion 12.3e-6 (1/℃), zero temperature 20℃
- それぞれの板材にSectionを設定し、上記の材料を指定します。

    <img src="image/example12_02.png" width=30%>  

4. CopperとSteelのセクションの間の面に対し、”tie constraint”を設定します。CopperのsurfaceをTie1, Steelの内側surafaceをTie2として、それぞれsurfaceを作成し、これらをmaster/slaveとして、Constraintsを右クリック＞Createで、”tie constraint”を作成します。

    <img src="image/example12_03.png" width=30%>  

5. Stepsで”coupled temperature-displacement step "を定義します。(Stepsを右クリック>Create>type "coupled temperature-displacement step "を選択)します。その他の設定はdefaultのままです。  

6. 境界条件を設定します。"BCs"で、バイメタルの片方の断面を完全拘束（Fixed）、それ以外の空気と接する表面に80℃と設定（Temperature）します。

    <img src="image/example12_04.png" width=30%>

6. 解析を実行します（Analysis-1を右クリック＞Runを選択）。 y方向の変位量の解析解は0.3807㎜ですが、解析結果は大きい結果となりました。

    <img src="image/example12_05.png" width=30%>  



