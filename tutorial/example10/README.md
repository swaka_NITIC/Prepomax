# Buckling of a simply-supported plate in compression
圧縮を受ける単純支持板材（200x150mm）の圧縮座屈の線形解析を実施します。

# 形状モデルの読み込みと解析実行手順
1. PrePoMaxを起動し、[mm,  ton,  s, °C]単位のプロジェクトファイルを新規作成します（File > New）。  

2. ダウンロードしたGeometryファイルの中から"Example 10 - plate compression buckling.stp"をimportします（File > import）。
　次に、Meshing Parametersの設定を変更して、最大要素サイズ(Max element size)を5mmに変更し、さらに"quad-dominated mesh"を有効化（Yes）してからメッシュ生成を行ってください。

    <img src="image/example10_01.png" width=30%>  

3. 材料を指定します。Materialsを右クリック>Create>Elsasticでヤング率200,000 MPa, ポアソン比0.3とします。さらに、Sectionを作成して（Sectionsを右クリック＞type "Shell section"）、この材料を指定します。シェルの厚さ(thickness）4mmも指定します。

    <img src="image/example10_02.png" width=30%>  

4. Stepsで”buckle step"を定義します。(Stepsを右クリック>Create>type "buckle step"を選択)します。number of buckling factors を3とします。  

6. 拘束条件を設定します。”BCs”で、４つの辺全体に面以外の変位拘束(U3=Fixed)の設定を行います。次に長辺と短辺を１つずつに”Displacement/Rotation”を設定し、辺に垂直方向の変位を拘束します。  

    <img src="image/example10_03.png" width=30%>

6. さらに荷重条件を設定します。残りの２辺に対し、”Loads”で"Normal shell edge load”を選択し,Edge load = 1N/mmを各辺の垂直方向に設定します。

    <img src="image/example10_04.png" width=30%>

7. 解析を実行します（Analysis-1を右クリック＞Runを選択）。メニュー上部のStep, Incrementで座屈モードを切り替えて確認することができます・・・・が、example manualの設定どおりだとv1.2.1ではエラーになります。検証中です。

    <img src="image/example10_05.png" width=30%>  

(3/16) @mmer547さんに検証していただき、どうも.inpファイル（calculixの入力ファイル）の出力バグのようです。出力された.inpの荷重条件設定（*DLOAD）の行にあるEDNOREDNORをEDNORに書き換えると計算が実行できることを確認しました。


