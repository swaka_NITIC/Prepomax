# Pressure loaded ring segment
リング状物体形状（外径150mm, 内径90mm, 厚さ20ｍｍ）の1/4セグメントモデルに対し、圧力荷重が作用する場合の静応力解析を実施します。

# 形状モデルの読み込みと解析実行手順
1. PrePoMaxを起動し、[mm,  ton,  s, °C]単位のプロジェクトファイルを新規作成します（File > New）。  

2. ダウンロードしたGeometryファイルの中から"Example 1 - ring.stp"をimportします（File > import）。  
    <img src="image/example01_01.png" width=30%>　

3. Meshing Parametersの設定を変更し、最大要素サイズ(Max element size)のみ3mmに変更してメッシュ生成を行います（Mesh > Meshing Parameters, Mesh > Create Mesh）。  
    <img src="image/example01_02.png" width=30%>　

4. 底面にNode set "Fix"を作成します。また、反対の端面にSurface "press"を作成します。  
    <img src="image/example01_03.png" width=30%>  　

5. 材料の新規作成を行います。名前を"Steel"、ヤング率210000MPa、ポアソン比0.3。さらに、section”Steal_section”を作成し、作成した材料"Steel"を指定します。  
    <img src="image/example01_04.png" width=30%>  

    <img src="image/example01_05.png" width=30%>  

6. Stepsで”静応力解析（static analysis）”を定義(Stepsを右クリック>Create>type "static step"を選択)します。  
    <img src="image/example01_06.png" width=30%>  

7. 拘束条件を追加します。BCsの所を右クリック＞Createを選択し、完全拘束(type ”Fixed”）を選んで、Node set”fix” に設定して、OKボタンを押します。  
    <img src="image/example01_07.png" width=30%>  

8. 荷重条件を追加します。Loadsの所を右クリック＞Createを選択し、圧力（type "Pressure"）を選んで、Surface name”press”に設定し、OKボタンを押します。圧力の値は「20 MPa」とします。  
    <img src="image/example01_08.png" width=30%>  

9. 解析を実行します（Analysis-1を右クリック＞Runを選択）。解析が終わるまで数秒かかります。問題なく実行できればFinishedになります。  
    <img src="image/example01_09.png" width=30%>  

10. 解析結果"Results"をクリックすると、VM応力(MISES)や変位量（DISP）を見ることができます。VM応力の解析解（理論解）は230.68MPaですが、これと比べてみましょう(?)。ファイルメニューのTools>Settings>Post-processingを選ぶと、Deformation scale factor等を変更できます。  
    <img src="image/example01_10.png" width=30%>  
     
    > Analytically calculated maximum stress value is 230.68 MPa 
    > and similar stresses can be found in simulation results 
    > when checking the outer shorter edge of the fixed face.
     
     と説明書きがありますが、特異点ではないのか？



11. 解析結果はFile>Saveで.pmx形式で保存できますし、様々な形式にexportも可能です。  



