# Elliptical bar torsion
楕円形状断面の梁（長径200mm, 短径100mm, 長さ1000ｍｍ）のモデルに対し、端面にねじり(torsion)が作用する場合の静応力解析を実施します。

# 形状モデルの読み込みと解析実行手順
1. PrePoMaxを起動し、[mm,  ton,  s, °C]単位のプロジェクトファイルを新規作成します（File > New）。  

2. ダウンロードしたGeometryファイルの中から"Example 3 - elliptical bar.stp"をimportします（File > import）。  
    <img src="image/example03_01.png" width=30%>　

3. Meshing Parametersの設定を変更し、最大要素サイズ(Max element size)のみ10mmに変更してメッシュ生成を行います（Mesh > Meshing Parameters, Mesh > Create Mesh）。  
    <img src="image/example03_02.png" width=30%>　

4. 材料の新規作成を行います。名前を"Steel"、ヤング率210000MPa、ポアソン比0.3。さらに、Solid section ”Steal_section”を作成し、作成した材料"Steel"を指定します。  
    <img src="image/example03_03.png" width=30%>  

5. Stepsで”静応力解析（static analysis）”を定義(Stepsを右クリック>Create>type "Static step"を選択)します。  
    <img src="image/example03_04.png" width=30%>  

6. トルクを加える参照点(reference point)を座標（0,0,1000）：先端面の楕円断面中心に作成します。 ”Reference points”を右クリック＞Create＞CoodinatesにX=0, Y=0, Z=1000mmを入力してください。   
    <img src="image/example03_05.png" width=30%>  

7. さらに拘束条件を追加します。Constraintsの所を右クリック＞Createを選択し、”Rigid body"を選びます。さらに、”Control point”で先ほど作成したReference point名を指定し、先端断面Surface選択してOKボタンを押します。  
    <img src="image/example03_06.png" width=30%>  

8. 境界条件として、根本断面を完全拘束（Fixed）、先端断面にモーメント荷重（moment load）M3=1,000,000Nmm（作用点は作成したreference pointとします）を作成します。  
    <img src="image/example03_07.png" width=30%>  

※ ここまでのキャプチャの図で、上述4のSolid section作成忘れてキャプチャしておりました。申し訳ありません。手順の最初からやらなくても追加で設定できます。

9. 解析を実行します（Analysis-1を右クリック＞Runを選択）。解析が終わるまで30数秒かかります。問題なく実行できればFinishedになります。  
    <img src="image/example03_08.png" width=30%>  

10. 解析結果"Results"をクリックすると、VM応力(MISES)や変位量（DISP）を見ることができます。せん断応力(今回はS13成分）の解析解は2.547MPaですが、メニューのTools>Query>Point/Nodeで梁下面の節点値を調べることができます。解析上の最大値2.555MPaとなり似た数値が得られました。  
    <img src="image/example03_09.png" width=30%>  
     
