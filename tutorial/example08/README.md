# Hertz contact of two spheres
2つの弾性球間の[ヘルツ接触](https://ja.wikipedia.org/wiki/%E3%83%98%E3%83%AB%E3%83%84%E3%81%AE%E6%8E%A5%E8%A7%A6%E5%BF%9C%E5%8A%9B)を解析します。
対称性から1/8モデルを利用して解析しています。

# 形状モデルの読み込みと解析実行手順
1. PrePoMaxを起動し、[mm,  ton,  s, °C]単位のプロジェクトファイルを新規作成します（File > New）。  

2. ダウンロードしたGeometryファイルの中から"Example 8 - Hertz contact.stp"をimportします（File > import）。二つSolid partがimportされます。  
　次に、Meshing Parametersの設定を変更して最大要素サイズ(Max element size)のみ5mmに変更してください。以上の作業の上、2つの1/8球でそれぞれメッシュ生成を行います（Mesh > Meshing Parameters, Mesh > Create Mesh）。  
　さらに、球の接触部を詳細に解析するため、local mesh refineを行います。Geometryタブで、"Mesh refinements"を右クリック＞Create で、接触する点を２つ選択します。Hide/Showを使ってうまく接触点をそれぞれのpartに設定します。メッシュサイズは0.5mmとします。 設定した後、さらにMesh_refinementを右クリックしactivateするとその部分のmesh refinementが有効化されます（節点近傍のメッシュが細分化されてます）。  
　 
    <img src="image/example08_01.png" width=30%>  
    <img src="image/example08_02.png" width=30%>  

3. 材料を指定します。Materialsを右クリック>Create>Elsasticでヤング率20000MPa, ポアソン比0.3とします。さらに、２つの球それぞれにSolid sectionを作成して（Sectionsを右クリック＞type "Solid section"）、この材料を指定します。  

    <img src="image/example08_03.png" width=30%>  

4. 接触面同士のinteractionを設定します。”Contact"中の”Surface interaction”を右クリック＞Surface behaviorを追加し、Type "hard"とする。さらに"Contact pairs"でMaster region、Slave regionで接触する球表面を選択します。  

    <img src="image/example08_04.png" width=30%>  
    <img src="image/example08_05.png" width=30%>  

5. Stepsで”statoc step"を定義します。(Stepsを右クリック>Create>type "Static step"を選択)します。この時、NlgeomをOn（defaultではoff)にします（幾何学的非線形性を考慮します）。  

6. 拘束条件を設定します。”BCs”で対称性を仮定してカットした6つの断面にそれぞれ” Displacement/Rotation boundary condition ”を設定し、それぞれの面の垂直方向変位を”Fixed”と設定して下さい。  

    <img src="image/example08_06.png" width=30%>

6. さらに拘束条件を設定します。”BCs”で対称性を仮定してカットした6つの断面にそれぞれ”Displacement/Rotation boundary condition”を追加設定し、上の球の上面にU2=-2mm（y方向）、下の球の下面にU2=2mm（y方向）の変位を設定します。これにより上下の球が相互に押されることになります。  

    <img src="image/example08_07.png" width=30%>

7. 解析を実行します（Analysis-1を右クリック＞Runを選択）。解析が終わり、解析結果"Results"をクリックすると、VM応力(MISES)の分布などを見ることができます。ヘルツ接触の解析解(接触点でのNormal方向の応力)は2798.3MPaですが、解析結果は2750MPaとなりました。メッシュ依存性が大きい問題ですので、接触点近傍をさらに細かくする（0.5>0.25>0.125mm)とどうなるか見てみましょう！ また、解析結果がおかしい場合は、"BCs"の設定をよく確認してみて下さい。 

    <img src="image/example08_08.png" width=30%>  
