# Shaft with surface traction loading
直径50mm, 長さ200mmの中実丸棒の端面に引張力が作用する場合の静応力解析を実施します。手順はExample01とほぼ同様です。

# 形状モデルの読み込みと解析実行手順
1. PrePoMaxを起動し、[mm,  ton,  s, °C]単位のプロジェクトファイルを新規作成します（File > New）。  

2. ダウンロードしたGeometryファイルの中から"Example 2 - shaft traction.stp"をimportします（File > import）。  
    <img src="image/example02_01.png" width=30%>　

3. Meshing Parametersの設定を変更し、最大要素サイズ(Max element size)のみ4mmに変更してメッシュ生成を行います（Mesh > Meshing Parameters, Mesh > Create Mesh）。  
    <img src="image/example02_02.png" width=30%>　

4. 材料の新規作成を行います。名前を"Steel"とし、ヤング率210000MPa、ポアソン比0.3。さらに、Solid Section ”Steal_section”を作成し、作成した材料"Steel"を指定します。  
    <img src="image/example02_03.png" width=30%>   

5. Stepsで”静応力解析（static analysis）”を定義(Stepsを右クリック>Create>type "static step"を選択)します。さらに、両端面のうち、片方を完全拘束(type Fixed)とし、もう片方の面に面引張力（Surface traction）F2＝-500N（y軸方向）加える設定とします。  
    <img src="image/example02_04.png" width=30%>  

6. 解析を実行します（Analysis-1を右クリック＞Runを選択）。解析が終わるまで10数秒かかります。問題なく実行できればFinishedになります。  

10. 解析結果"Results"をクリックし、VM応力(MISES)や変位量（DISP）を見ることができます。VM応力の解析解（理論解）は8.17MPaですが、これと比べてみましょう。ファイルメニューのTools>Settings>Post-processingを選ぶと、Deformation scale factor等をUser definedに変更でき、設定値を400に変え、" draw undeformed model"オプションをYESにして変位を表示させてみてください。   
    <img src="image/example02_05.png" width=30%>   
    <img src="image/example02_06.png" width=30%>   





