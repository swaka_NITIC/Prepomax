# Beam modal analysis
単純指示梁（□40mm、長さ1000mm）のモーダル解析（固有振動数解析）を実施してみます。

# 形状モデルの読み込みと解析実行手順
1. PrePoMaxを起動し、[mm,  ton,  s, °C]単位のプロジェクトファイルを新規作成します（File > New）。  

2. ダウンロードしたGeometryファイルの中から"Example 6 - beam modal.stp"をimportします（File > import）。さらに、Meshing Parametersの設定を変更して最大要素サイズ(Max element size)のみ10mmに変更の上、メッシュ生成を行います（Mesh > Meshing Parameters, Mesh > Create Mesh）。
    <img src="image/example06_01.png" width=30%>　

3. 材料の新規作成を行います。名前を"Steal"、ヤング率210000MPa、ポアソン比0.3とします。この時、Density設定も追加し、7.85e-9 t/mm3の値も設定します。さらにSolid sectionを追加し、この材料を指定します。  
    <img src="image/example06_02.png" width=30%>  

4. Stepsで”Frequency step"を定義します。(Stepsを右クリック>Create>type Frequency step"を選択)します。Number of frequenciesの部分は、固有振動モードの計算する最大次数で、ここでは10のままでOKです。  
    <img src="image/example06_03.png" width=30%>  

5. 拘束条件を設定します。”BCｓ”で断面の片方の下端edgeに、” Displacement/Rotation boundary condition ”を設定（変位U1,U2,U3をFixed, UR1,UR2,UR3はUnconstrainedのままでOKです）します。  
    <img src="image/example06_04.png" width=30%>

6. さらに拘束条件を設定します。”BCｓ”で反対側断面の片方の下端edgeに、” Displacement/Rotation boundary condition ”を設定（変位U1,U2をFixed, U3,UR1,UR2,UR3はUnconstrainedのままでOKです）します。上記5と設定が異なりますので注意して下さい。これは梁長手方向の変位のみを許容することになります。  
    <img src="image/example06_05.png" width=30%>  
    <img src="image/example06_06.png" width=30%>

7. 解析を実行します（Analysis-1を右クリック＞Runを選択）。解析が終わり、解析結果"Results"をクリックすると、VM応力(MISES)や変位量（DISP）を見ることができます。また、振動の次数を変えるには、ウインドウ上部のPrevious/Nextの部分のボタンを押します。
    <img src="image/example06_07.png" width=30%>   
     
8. 解析した固有振動数[Hz]と、解析解（2D面内変位を仮定）との比較すると以下のような結果になりました（3Dでのモード次数3までの対応のみ比較です。解析解は2D変位のみ考慮しているため、3D振動モードで現れる捩じりや他の面内振動モードを考慮できません）。

    |No.|Analytic|Calculated|
    |---|--------|----------|
    | 1 |93.82|93.15|
    | 2 |375.46|364.13|
    | 3 |844.07|766.10|





