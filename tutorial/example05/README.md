# Assembly of two cylinders
２つの大小円柱（直径80mm,長さ100mm）,(直径50mm, 長さ120mm)の接触静応力解析を実施してみます。円柱同士の接触面の設定部分が新規の内容になります。

# 形状モデルの読み込みと解析実行手順
1. PrePoMaxを起動し、[mm,  ton,  s, °C]単位のプロジェクトファイルを新規作成します（File > New）。  

2. ダウンロードしたGeometryファイルの中から"Example 5 - cylinder assembly.stp"をimportします（File > import）。Geometryタブの２つのSolid_partを右クリックするとHide/Showを切り替えられますので、どのような形状か確認してみましょう。  
    <img src="image/example05_01.png" width=30%>　

3. 【注意】２つのSolid_partそれぞれにおいて、Meshing Parametersの設定を変更し、最大要素サイズ(Max element size)のみ5mmに変更してメッシュ生成を行います（Mesh > Meshing Parameters, Mesh > Create Mesh）。  
    <img src="image/example05_02.png" width=30%>　

4. 材料の新規作成を行います。名前を"Plastic"、ヤング率180000MPa、ポアソン比0.25とします。さらに、２つのSolid sectionそれぞれに作成した材料を指定します。  
    <img src="image/example05_03.png" width=30%>  

5. 接触部分”Contact”の設定をします。まず、細い円柱（緑色）の後側端面に"Tie1"、太い円柱（灰色）の前側端面に"Tie2”という名前のSurfaceを設定します。次に、Constraintsを右クリック>CreateでType "Tie”で、mastar surfaceにTie1, slave surfaceにTie2と上記の名前で指定してOKを押します。  
    <img src="image/example05_04.png" width=30%>  
    <img src="image/example05_05.png" width=30%>
 
6. Stepsで”静応力解析（static analysis）”を定義(Stepsを右クリック>Create>type "Static step"を選択)します。 

7. 細い円柱の前側断面にPressure load(magnitude 30MPa), 太い円柱の後側断面を完全拘束(Fixed)として、境界条件を設定します。  
    <img src="image/example05_06.png" width=30%>

9. 解析を実行します（Analysis-1を右クリック＞Runを選択）。解析が終わり、解析結果"Results"をクリックすると、VM応力(MISES)や変位量（DISP）を見ることができます。  
※ Warning画面が出るようですが、そのまま「はい」で問題ありません。  

    <img src="image/example05_07.png" width=30%>  
    <img src="image/example05_08.png" width=30%>  
     
