# Cylindrical shell buckling
圧縮を受ける薄肉円筒シェル（直径300mm, 長さ600㎜）の線形座屈解析を実施します。

# 形状モデルの読み込みと解析実行手順
1. PrePoMaxを起動し、[mm,  ton,  s, °C]単位のプロジェクトファイルを新規作成します（File > New）。  

2. ダウンロードしたGeometryファイルの中から"Example 9 - shell buckling.stp"をimportします（File > import）。
　次に、Meshing Parametersの設定を変更して、最大要素サイズ(Max element size)を10mmに変更し、さらに"quad-dominated mesh"を有効化（Yes）してからメッシュ生成を行ってください。

    <img src="image/example09_01.png" width=30%>  

3. 材料を指定します。Materialsを右クリック>Create>Elsasticでヤング率21000MPa, ポアソン比0.3とします。さらに、Sectionを作成して（Sectionsを右クリック＞type "Shell section"）、この材料を指定します。シェルの厚さ(thickness 5mm)も指定します。

    <img src="image/example09_02.png" width=30%>  

4. reference point (0,0,600)を作成します。このreference pointを用いて、円筒シェルのエッジに対して、rigid body constraintを設定します。

    <img src="image/example09_03.png" width=30%>  

5. Stepsで”buckle step"を定義します。(Stepsを右クリック>Create>type "buckle step"を選択)します。number of buckling factors を5とします。  

6. 拘束条件を設定します。”BCs”で円筒下部のエッジに完全拘束（Fixed）を設定します。  

    <img src="image/example09_04.png" width=30%>

6. さらに荷重条件を設定します。”Loads”でConcentrated force(集中荷重）とし、Regionは、Reference point nameを指定し、先に作成したreference pointを指定します。

    <img src="image/example09_05.png" width=30%>

7. 解析を実行します（Analysis-1を右クリック＞Runを選択）。 deformation scale factorを10000に設定します。メニュー上部のStep, Incrementで座屈モードを切り替えて確認することができます。
（参考:[薄肉円筒殻の座屈について](http://psds.co.jp/seisdesign/t04.pdf)） 

    <img src="image/example09_06.png" width=30%>  

