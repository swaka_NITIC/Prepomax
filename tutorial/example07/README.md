# Elasto-plastic plate in tension
丸穴の空いた弾塑性板材に引張力を加えた状態を解析します。
もともとの板サイズは300x150mm, 穴径60mm, 厚さ10mmですが、対称性から1/4モデルを利用して解析しています。

# 形状モデルの読み込みと解析実行手順
1. PrePoMaxを起動し、[mm,  ton,  s, °C]単位のプロジェクトファイルを新規作成します（File > New）。  

2. ダウンロードしたGeometryファイルの中から"Example 7 - elasto-plastic plate.stp"をimportします（File > import）。さらに、Meshing Parametersの設定を変更して最大要素サイズ(Max element size)のみ4mmに変更の上（テキスト中は2mmになってるのですが記載図に合わせるとすれば4mmのtypoと思われます）、メッシュ生成を行います（Mesh > Meshing Parameters, Mesh > Create Mesh）。  
    <img src="image/example07_01.png" width=30%>　

3. 材料の新規作成を行います。ヤング率210000MPa、ポアソン比0.3とします。この時、Plasicity(塑性)設定も追加し、以下のような値も設定します。さらにSolid sectionを追加し、この材料を指定します。  

|Yield stress [MPa]|Plastic strain[-]|
|------------------|-----------------|
|235|0|
|335|0.12|
 
 <img src="image/example07_02.png" width=30%>  

4. Stepsで”statoc step"を定義します。(Stepsを右クリック>Create>type "Static step"を選択)します。 

5. 拘束条件を設定します。”BCｓ”で対称性を仮定してカットした２つの断面の” Displacement/Rotation boundary condition ”を設定します。変位については、それぞれの垂直方向を”Fixed”と設定して下さい。  
    <img src="image/example07_03.png" width=30%>

6. さらに拘束条件を設定します。”BCｓ”で板の表・裏面にも5.と同様に垂直方向の変位拘束を追加します（これによりX-Y面内の２次元解析を行っていることになります）。  

    <img src="image/example07_04.png" width=30%>

7. 荷重条件を設定します。”Loads”で上側段面にtype "Pressure"として-200 MPaを設定します。  
    <img src="image/example07_05.png" width=30%>

8. ”Field output"右クリック＞Create＞Type "Element output"を選び、Propertieｓ＞Variables to outputで「PEEQ（Equivalent plastic strain）」を追加します。  
    <img src="image/example07_06.png" width=30%>  

9. 解析を実行します（Analysis-1を右クリック＞Runを選択）。解析が終わり、解析結果"Results"をクリックすると、VM応力(MISES)の応力集中の様子や、塑性歪み（Plastic strain）を見ることができます。また、メニュー>"Results">Transformationでx方向、ｙ方向のsymmetryを設定するとモデル全体の結果表示できます。  
    <img src="image/example07_07.png" width=30%>  
    <img src="image/example07_08.png" width=30%>  
     

