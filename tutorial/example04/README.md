# Rectangular bar subjected to torsion and bending
矩形形状断面の梁（200x300mm, 長さ1400ｍｍ）のモデルに対し、端面にねじり(torsion)と曲げ荷重(bending)が作用する場合の静応力解析を実施します。

# 形状モデルの読み込みと解析実行手順
1. PrePoMaxを起動し、[mm,  ton,  s, °C]単位のプロジェクトファイルを新規作成します（File > New）。  

2. ダウンロードしたGeometryファイルの中から"Example 4 - rectangular bar.stp"をimportします（File > import）。  
    <img src="image/example04_02.png" width=30%>　

3. Meshing Parametersの設定を変更し、最大要素サイズ(Max element size)のみ30mmに変更してメッシュ生成を行います（Mesh > Meshing Parameters, Mesh > Create Mesh）。  
    <img src="image/example04_02.png" width=30%>　

4. 材料の新規作成を行います。名前を"Plastic"、ヤング率180000MPa、ポアソン比0.25。さらに、Sectionsで"Solid_section”を作成し、Materialに作成した材料"Plastic"を指定します。  
    <img src="image/example04_03.png" width=30%>  

5. Stepsで”静応力解析（static analysis）”を定義(Stepsを右クリック>Create>type "Static step"を選択)します。  

6. Example03と同様にトルクを加える参照点(reference point)を座標（500,0,1400）に作成します。 ”Reference points”を右クリック＞Create＞CoodinatesにX=500, Y=0, Z=1400mmを入力してください。   
    <img src="image/example04_04.png" width=30%>  

7. さらに拘束条件を追加します。Constraintsの所を右クリック＞Createを選択し、”Rigid body"を選びます。さらに、”Control point”で先ほど作成したReference point名を指定し、先端断面Surface選択してOKボタンを押します。  
    <img src="image/example04_05.png" width=30%>  

8. 境界条件として、根本断面を完全拘束（Fixed）、先端断面に集中荷重（load）F1=0, F2=-8000N F3=0（作用点は作成したreference pointとします）を作成します。  
    <img src="image/example04_06.png" width=30%>  

9. 解析を実行します（Analysis-1を右クリック＞Runを選択）。解析が終わるまで10数秒かかります。問題なく実行できればFinishedになります。  
    <img src="image/example04_07.png" width=30%>  

10. 解析結果"Results"をクリックすると、VM応力(MISES)や変位量（DISP）を見ることができます。deformation scale facor を1000としてみてください。vM応力の解析解は最大値4.69MPaですが、Queryを使って調べた解析上の最大値のすぐ隣の接点値は4.77MPaとなり似た数値が得られました。  
    <img src="image/example04_08.png" width=30%>  
     

