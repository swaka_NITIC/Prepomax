# 片持ち梁の線形静解析


## 解析モデル
- 長さl=500mm, 長方形断面(幅b=30mm, 高さh=20mm)を持つ片持ち梁（beam）です。
- 解析モデルのCADデータはこちらです。 
    - [SLDPRT形式(SolidWorks)](./cadData/BeamL500b30h20.SLDPRT)
    - [STEP形式](./cadData/)  
- 片方の端面を拘束保持し、反対側の端面に（平行に）下向き荷重を加えます。今回は自重は考慮しません。
  
    <img src="./image/tut01_01.png" width="30%"> 

## 形状モデルの読み込み
1. STEP形式のダウンロード  
    - 上記リンクからダウンロードして適当な場所に保存して下さい。  
2. PrePoMaxの起動とプロジェクトの新規作成 
    - PrePoMaxを起動し、メニュー＞File＞Newを選び、新規プロジェクトを作成します。  
    - 2D/3Dか、単位系などを選ぶウインドウが出てきますので、ここではModel Spaceで"3D"、Unit System Typeで"mm、ton、s、℃"を選び（長さの単位にmm, 重さの単位にton, 時間の単位にs, 温度の単位に℃を使うということです）、右下のOKを選びます。  
    - PrePoMaxのメインウインドウ右下に単位系が表示されていることを確認して下さい。  
        <img src="./image/tut01_02.png" width="30%">  
        <img src="./image/tut01_03.png" width="30%">  

3. STEPファイルの読み込み  
    - File > Import からダウンロードしたSTEPファイルを読み込みます。
    - データファイルに問題なければ、上記の形状が表示されます。マウス中ボタンを押すか、カーソルキーで回転させてみましょう。マウスホイールで拡大・縮小できます。  
        <img src="./image/tut01_04.png" width="30%">  
    - 左側のGeometryタブには、"Solid_part-1"という表示が現れ、ソリッドのパーツモデルが１つ読み込まれたことを示しています。番号は順番に自動的につけられたものです。

## メッシュ生成 
- Geometryタブの"Solid_part-1"を右クリックし、"Meshing Parameters"を選択します。  
- 出てきたウインドウで、”Max element size” のみ5mmに変更します。右下の目のマークで、稜線の分割をプレビューできます。ここでは、メッシュの次数なども変更できますが、今回はデフォルトのまま（Second order）とします。 
- 問題なければ"OK"を押します。  
    <img src="./image/tut01_05.png" width="30%">  
- 再び、 Geometryタブの"Solid_part-1"を右クリックし、"Create Mesh"を選択します。
- メッシュ生成が開始され、成功すれば自動的に"FE Model"タブに切り替わります。右のモデル画面では、読み込んだ片持ち梁モデルがメッシュ分割（四面体ソリッド要素,Second order）されていることがわかります。  
    <img src="./image/tut01_06.png" width="30%">    
## 境界条件設定
- ”FE Model”タブでは、材料指定、荷重条件、拘束条件、接触条件、解析方法などを指定していきます。
- これらの指定を行うためには、あらかじめ「どこの部分か」を決めておきます。  
### 材料条件の指定領域作成
1. 材料指定のために、”Element sets”を作成します。”Element sets”を右クリック＞Createを選択します。2つウインドウが出てきます（Create Elemt Set, Set selection）。  
2. ”Set selection”中の"Part"をチェックし、右の片持ち梁をクリックすると、このモデルに含まれる要素がすべて選択され、ELement setが作成され、Create Elemt Setウインドウに選択した要素数が表示されます。Nameにある"Element_set-1"は変更可能ですが、今回はこのまま利用します。
3. Create Elemt Setウインドウの”OK”ボタンを押します（複数のパーツがあるようなモデルの場合は、"OK-New"を選択すると続けてElement setを作成できます）。
4. 修正したい場合は、作成されたElement setを右クリックし、"Edit"とすると再び上記２つのウインドウが出てきます。
### 拘束・荷重条件の指定領域作成
1. 拘束する面と、荷重を加える面を順に作成します。
2. "Surfaces"を右クリック＞Createを選択します。”Set selection"ウインドウは、一番上の"Surfaces edges and vertices"にチェックしたまま、拘束をかける面（片方の端面）を選択してください。Nameは”fix”と変更してOKボタンを押してください。
3. 次に荷重面のset ”load”を作成します。同様に、"Surfaces"を右クリック＞Createを選択します。拘束をかける面とは反対側の端面を選択してください。Nameは”load”と変更してOKボタンを押してください。

## 材料条件の指定
1. "Materials"を右クリック＞"Material library"を選択します。PrePoMaxに登録済みの材料から、解析に使用するものを選び、→ボタンで、右側の"FE Model materials"に登録（移動）します。ここでは、解析に使用する材料選定を行っているだけで、具体的に各モデルの要素に材料指定するのは別の作業になります。
2. ここでは、Structualの中のS355を選択しで"OK"ボタンを押します（登録されている材料は欧州規格名のようですので、JISに対応する材料特性を別途確認する必要があります）。
3. もし新しい材料を使いたい場合は、 "Materials"を右クリック＞"Create"を選択します。物質名や、Density, Elaticity,　Plasticity, Thermal expansionn, Thermal conductivity, SPecific heatなどの数値を入力することになります。
4. ここまでの設定で、以下のようなツリーになっていると思います。  
        <img src="./image/tut01_07.png" width="30%">  
5. ようやく材料指定です。"Section"を右クリック＞"Create"を選択し、Typeの中から"Solid section"を、”Propertits”でMaterialに先ほど登録した"S355"を選択します。次いで、"Set selection”ウインドウで、"Part"にチェックし、モデル画面で片持ち梁を選択します（稜線が赤く変わり選択されたことがわかります）。これで"OK"ボタンを押してください。
6. ”Region type”で、"Element set name”とすると、先ほど指定したElement setの名前"Element_set-1"でも選択・指定可能です。

## 線形静解析の実行（拘束条件・荷重条件の指定を含む）
1. "Steps"を右クリック＞"Create"を選択すると、解析の種類を選ぶことができます。今回は、線形静解析に相当する"Static step"を選んでOKボタンを押してください。
2. "Name"や"Solver”などの欄はdefaultのままとします。
3. 拘束条件は、"BCs"を右クリック＞”Create”を選択し、Typeから"Fixed"を選びます。Region typeで"Surface name"を選ぶと、先に名前をつけた指定領域を選ぶことができるようになります。ここでは"fix"を選びOKボタンをおします。これで、面"fix"が完全拘束面として境界条件が指定されました(緑色の完全拘束を示すマークが出ます)。
4. 荷重条件は、"Loads"を右クリック＞”Create”を選択し、Typeから"Surface traction"を選びます。Region typeでは"Surface name"を選択し、先ほど指定した領域"load"を選びます。Force componentsに力のxyzを指定します。ここではF1=0N, F2=0N, F3=-100Nを選びOKボタンを押します（これで、荷重方向に力の青い矢印が出ます）。なお、荷重条件のtypeでは、圧力や引張力、重力なども選定できます。
5. ここまでで、左のModel設定ツリーは以下のようになっているはずです。  
        <img src="./image/tut01_08.png" width="30%">
6. 解析実行してみます。一番下に出てきた"Analysis-1"を右クリックし、"Run"を選択します。設定に問題なければ"Monitor"ウインドウで解析実行のログが表示され、ソルバーであるCalculixで解析が行われたことがわかります。ウインドウ下部で"Finished”が出てくれば解析終了です。
        <img src="./image/tut01_09.png" width="30%">

## 解析結果の可視化
1. "Monitor"ウインドウ下部の"Results"ボタンを押すと、解析結果が自動的に表示されます。  
        <img src="./image/tut01_10.png" width="30%">
2. 表示できる主なものは以下の通りです([マニュアル](https://prepomax.fs.um.si/wp-content/uploads/2022/02/PrePoMax-v1.2.1-manual.pdf)参照)。
    - DISP：合成変位とその３成分
    - STERESS：VM応力、応力テンソル６成分、最大/中間/最小主応力
    - TOSTRAIN: ひずみテンソル６成分
    - FORC: 内部発生力とその３成分
    - ERROR: エラー率
## 演習課題
1. 理論解との比較
    片持ち梁の変位解析解と比較して、解析結果はどれくらい異なるか調べてみましょう。
2. メッシュタイプの違いと解析値の比較
    メッシュサイズや、メッシュ次数を変更すると、計算結果はどの程度変わるか、合成変位およびＶＭ応力にそれぞれについて調査してみましょう。


