# PrePoMaxの利用方法


## [PrePoMax](https://prepomax.fs.um.si)とは？
- メッシュ生成に[Netgenライブラリ](https://ngsolve.org/)、
内部ソルバーに[Calculix](http://www.calculix.de/)を用い、CADデータのインポート、各種応力解析設定・解析実行、可視化までを行えるGPLv3フリーソフトウェアです。
- スロベニアのマリボー大学のMatej Borovinšek先生が開発されています。
- Windows版バイナリとソースコードのみ公開されています。
<img src="https://prepomax.fs.um.si/wp-content/uploads/2021/06/Interface_7.png" width="30%">
<img src="https://prepomax.fs.um.si/wp-content/uploads/2021/06/Interface_8.png" width="30%">
<img src="https://prepomax.fs.um.si/wp-content/uploads/2021/06/Interface_9.png" width="30%">

## 主な特徴（詳細はHPを確認下さい）
### 解析タイプ
- 線形静解析
- 非線形静解析(geometric, material and contact nonlinearities)
- 周波数解析
- 座屈解析
- 熱伝導解析
- 連成・非連成熱変位解析（熱応力・熱膨張）
### メッシュ生成
- シェル要素
- stl, step, igesファイル等のCADデータファイルおよびメッシュの読み込み（shell, solid）
- メッシュ細分化（節点・辺・面ベース）
### 要素タイプ
- １次・２次三角形シェル要素
- １次・２次四角形シェル要素
- １次・２次四面体ソリッド要素
- １次・２次くさびソリッド要素（importのみ）
- １次・２次六面体ソリッド要素（importのみ）
### アセンブリ解析機能
- 剛体結合モデル
- 摩擦あり接触面
### 材料モデル
- ビルトイン材料ライブラリ
- 線形弾性モデル（温度依存）
- 弾塑性モデル（温度依存）

## ダウンロード&インストール
- [こちらのダウンロードページ](https://prepomax.fs.um.si/downloads/)からWindows版バイナリzipファイルをダウンロードして下さい。

<img src="image/top/top_01.png" width=50%>

- 2024年8月時点で最新v2.1.4-dev
- Microsoft .NET Framework 4.5.1が必要ですが、大抵のWindowsにはすでに入っているはずです。
- ダウンロードしたzipファイルを適当な場所に解凍し、中にあるPrePoMax.exeを起動します（いわゆるインストール作業は不要です）。

<img src="image/top/top_02.png" width=20%>　　<img src="image/top/top_03.png" width=35%>

- :exclamation: フォルダパスに日本語が含まれていると正しく動作しない場合があります。また、初回の起動でWindowsシステムにより「保護されました」という警告が出る場合がありますが、そのまま実行して下さい。

## チュートリアル
### [片持ち梁の線形静解析](./tutorial/simpleBar/README.md)

## 解析Example
PrePoMaxサイトの[Documentationページ](https://prepomax.fs.um.si/documentation/)にあるAdditional exampleの解析を自分なりにやってみました。多少補足している部分もあります。  
あらかじめ、[Exaｍple manual](https://prepomax.fs.um.si/wp-content/uploads/2021/09/2021.09.07-PrePoMax-v1.1.0-examples-manual.pdf)と[Geometry Files](https://prepomax.fs.um.si/wp-content/uploads/2021/09/2021.09.07-PrePoMax-examples-geometries.zip)をダウンロードしてお使いください。  

1. [Pressure loaded ring segment(圧力のかかったリングセグメント)](./tutorial/example01/README.md)  
2. [Shapt with surface traction loading(表面引張荷重のかかる軸)](./tutorial/example02/README.md)   
3. [Elliptical bar torsion(楕円断面の梁のねじり)](./tutorial/example03/README.md) 
4. [Rectangular bar subjected to torsion and bending(ねじりと曲げを受ける矩形断面梁))](./tutorial/example04/README.md)   
5. [Assemply of two cylinders(圧力を受ける２重シリンダアセンブリ)](./tutorial/example05/README.md)  
6. [Beam modal analysis(梁のモーダル解析)](./tutorial/example06/README.md)  
7. [Elasto-plastic plate in tension(引張をうける弾塑性プレート解析)](./tutorial/example07/README.md)  
8. [Hertz contact of two spheres(２つの球のヘルツ接触解析)](./tutorial/example08/README.md)  
9. [Cylindrical shell buckling(薄肉円柱の座屈解析)](./tutorial/example09/README.md)  
10. [Buckling of a simply-supported plate in compression(支持を受ける板材の座屈解析)](./tutorial/example10/README.md)   
11. [Heat transfer in insulated pipeline(断熱パイプの熱伝導解析)](./tutorial/example11/README.md)   
12. [Termomechanical analysis of a bimetallic strip(バイメタルの熱変形解析)](./tutorial/example11/README.md)   

## 参考サイト
- [PrePoMax本家サイト](https://prepomax.fs.um.si/)
- [PrePoMax Tutorial Videos](https://www.youtube.com/watch?v=nGUzVGwSUrk&list=PLaWK58rtjzvS2ZKClbldRYN8Iu_gTmsC0)
- [Qiita: PrePoMax使用方法解説](https://qiita.com/Sagittarius_Chiron/items/fd806361ceca4864bc79)
- [PrePoMaxマニュアル日本語版(機械翻訳)](https://speakerdeck.com/juntatsuno/prepomax-v1-dot-1-1-maniyuaru?slide=7)
- [岐阜高専・柴田先生](http://opencae.gifu-nct.ac.jp/pukiwiki/index.php?AboutPrePoMax)
- [Qiita@Jun_Tatsunoさん](https://qiita.com/Jun_Tatsuno/items/9e0545d437e017babfe9)
